"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
extends Node

# todo: Easy way for keeping track of available doc options/ideas is to have list of already picked options and subtract it

# todo: Move to FillingScene.gd?
const FILLING_TYPE_TO_SCENE := {
  "textbox": preload("res://scenes/filling/Textbox.tscn"),
  "textbox-big": preload("res://scenes/filling/TextboxBig.tscn"),
  "selector": preload("res://scenes/filling/Selector.tscn")
}

onready var n_Split := find_node("SplitContext")
onready var n_OptionsItems := find_node("OptionItems")
onready var n_History := find_node("History")
onready var n_HistoryItems := find_node("HistoryItems")

var project: Project
var choice_context: ChoiceContext setget _set_choice_context

# Last history element that was displayed, used to not rebuild the whole history screen every time something is added
# todo: Make sure that it will not be desynced from project state edited in non-obvious ways, probably by definiting connection on history change
var _last_history: int = -1


func _ready() -> void:
  System.check(get_tree().get_root().connect("size_changed", self, "_update_orientation") == OK)
  _update_orientation()
  self.project = Project.new()
  self.project.add_flavor("So... time to create something, I suppose. But with what should we start?")
  _next_option_doc_list()
  _update_history()


func _next_option_doc_list() -> void:
  self.choice_context = ChoiceContext.new().init_doc_list(fetch_random_options(3))


func _on_option_picked(doc: DocOption) -> void:
  assert(choice_context.type == ChoiceContext.DOC_LIST)  
  self.n_History.call_deferred("scroll_to_bottom")
  if doc.context == null:
    self.project.add_option(doc)
    _update_history()
    _next_option_doc_list()
  else:
    self.choice_context = ChoiceContext.new().init_doc_filling(doc)


func _on_filling_finished(_arg) -> void:
  # todo: Way to denote fields as unnecessary for filling up
  # todo: It might be better not to assume position of FillingScene nodes, but register then in sequence on creation in array local to Board
  # todo: Binding to names which will allow usage of filled data outside of stored option
  assert(self.choice_context.type == ChoiceContext.DOC_FILLING)
  var info := Array()
  # var bound_data := Dictionary()
  var node_idx := 1
  for field in self.choice_context.context.context:
    var node := self.n_OptionsItems.get_child(node_idx) as FillingScene
    if not node.is_filled():
      return
    var data = node.fetch()
    info.push_back(data)
    # if "bind" in field:
    #   assert(not "bind" in bound_data)
    #   bound_data[field["bind"]] = data # todo: Should it be allowed to be null?
    node_idx += 1
  self.n_History.call_deferred("scroll_to_bottom")
  self.project.add_option(self.choice_context.context, info)
  _update_history()
  _next_option_doc_list()


func _set_choice_context(cc: ChoiceContext) -> void:
  choice_context = cc
  for child in self.n_OptionsItems.get_children():
    self.n_OptionsItems.remove_child(child)
    child.queue_free()
  match choice_context.type:
    ChoiceContext.DOC_LIST:
      for idea in choice_context.context:
        var option = preload("res://scenes/OptionPicker.tscn").instance().init(idea)
        System.check(option.connect("picked", self, "_on_option_picked") == OK)
        self.n_OptionsItems.add_child(option)
    ChoiceContext.DOC_FILLING:
      var option_preview = preload("res://scenes/history/Option.tscn").instance().init(choice_context.context)
      self.n_OptionsItems.add_child(option_preview)
      for field in choice_context.context.context:
        assert("type" in field)
        assert(field["type"] in FILLING_TYPE_TO_SCENE, "Unknown filling field type %s" % field["type"])
        var filling := FILLING_TYPE_TO_SCENE[field["type"]].instance().init(field) as FillingScene
        self.n_OptionsItems.add_child(filling)
      var finish := preload("res://scenes/filling/Finish.tscn").instance()
      System.check(finish.connect("picked", self, "_on_filling_finished") == OK)
      self.n_OptionsItems.add_child(finish)
    _: push_error("Unknown choice context type")


func _update_history() -> void:
  # todo: Project should define history iterator object to hide array from editing directly
  #       Problem is that dictionaries are mutable and thus we would probably need to deep copy each iteration
  # todo: Really fragile method, if all project changes are done via here then we could have "update" method always receive what changed
  assert(self._last_history <= self.project._history.size())
  if self._last_history == self.project._history.size() - 1:
    return
  for item in self.project._history.slice(self._last_history + 1, self.project._history.size() - 1):
    match item["type"]:
      "flavor":
        var flavor = preload("res://scenes/history/FlavorText.tscn").instance().init(item["content"])
        self.n_HistoryItems.add_child(flavor)
      "option":
        var option = preload("res://scenes/history/Option.tscn").instance().init(item["content"], item["context"])
        self.n_HistoryItems.add_child(option)        
      _: push_error("Unknown project history type")
  self._last_history = self.project._history.size() - 1


func _update_orientation() -> void:
  if OS.window_size.x / OS.window_size.y >= 1.25:
    self.n_Split.orientation = SplitContext.Orientation.HORIZONTAL
  else:
    self.n_Split.orientation = SplitContext.Orientation.VERTICAL


static func fetch_random_options(count: int) -> Array:
  # todo: Options that were already used should also be 'grayed' out
  var result := Array()
  var gray_values := Array() # Refers to indexes that are already in use
  var ideas := Content.get_ideas()
  assert(ideas.size() >= count)
  while count > 0:
    var dice := System.gameplay_random.randi() % ideas.size()
    if gray_values.find(dice) == -1:
      result.push_back(Content.get_idea(ideas[dice]))
      gray_values.push_back(dice)
      count -= 1
  return result
