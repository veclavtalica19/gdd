"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
extends Control


func init(opt: DocOption, context = null) -> Control:
  assert(not opt.label.empty())
  $Label.text = opt.label
  if not opt.desc.empty():
    $Desc.text = opt.desc
    $Desc.show()
  if context != null:
    _reflect_context(opt, context)
  return self


func _reflect_context(opt: DocOption, context: Array) -> void:
  var items_node := find_node("ContextItems")
  var idx := 0
  for field in opt.context:
    match field["type"]:
      "textbox", "textbox-big":
        var node = preload("res://scenes/history/Textbox.tscn").instance().init(field["desc"], context[idx])
        items_node.add_child(node)
      # _: push_error("Unkown filling type %s" % field)
    idx += 1
