"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
extends Container


func init(desc: String, content: String) -> Container:
  # Workaround for passing the values to scene instantiated nodes
  set_meta("desc", desc)
  set_meta("content", content)
  return self


func _ready() -> void:
  $Field.text = get_meta("desc")
  var content := get_meta("content") as String
  if not content.empty():
    $Content.text = content
    $Content.show()
  remove_meta("desc")
  remove_meta("content")
