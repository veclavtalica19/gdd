"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
extends Picker

onready var n_Description := find_node("Desc")
onready var n_Label := find_node("Label")
onready var n_EditContextHint := find_node("EditContextHint")

var _descriptor: DocOption = null
var _is_hovered: bool = false


func init(d: DocOption) -> Control:
  self._descriptor = d
  self._picked_arg = d # To return on signal
  return self


func _ready() -> void:
  _hide_description()
  _reflect_descriptor()
  System.check(connect("mouse_entered", self, "_on_hover") == OK)
  System.check(connect("mouse_exited", self, "_on_unhover") == OK)


func _reflect_descriptor() -> void:
  assert(self._descriptor != null)
  self.n_Label.text = self._descriptor.label
  self.n_Description.text = self._descriptor.desc
  if self._descriptor.context != null:
    self.n_EditContextHint.show()


func _hide_description() -> void:
  self.n_Description.hide()


func _show_description() -> void:
  if self.n_Description.text.length() != 0:
    self.n_Description.show()


func _on_hover() -> void:
  self._is_hovered = true
  _show_description()


func _on_unhover() -> void:
  self._is_hovered = false
  _hide_description()
