"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
extends FillingScene

# todo: Make check mark show up on selection on items, while disabling every other item for clicking

const N_DESC_PATH = "HBoxContainer/Desc"
const N_CHOICES_LEFT_PATH = "HBoxContainer/ChoicesLeft"
const N_ITEM_CONTAINER = "ItemContainer"

var _desc: String
# How many items are supposed to be selected
var _selection_count: int
var _selected: Array # Array<String>
var _are_buttons_locked: bool = false


func init(args: Dictionary) -> FillingScene:
  if "desc" in args:
    self._desc = args["desc"]
  System.check("selection" in args)
  var selection := Content.expand_tags(args["selection"]) as Array
  if "selection-limit" in args:
    selection = System.rand_seq(System.gameplay_random, selection, args["selection-limit"])
  if "selection-count" in args:
    _selection_count = args["selection-count"]
  var n_item_container := get_node(N_ITEM_CONTAINER)
  for item in selection:
    var scene = preload("res://scenes/filling/SelectorItem.tscn").instance().init(item)
    System.check(scene.connect("selected", self, "_on_item_selected") == OK)
    n_item_container.add_child(scene)
  return self


func _ready() -> void:
  if not self._desc.empty():
    var n_desc := get_node(N_DESC_PATH)
    n_desc.text = self._desc
    n_desc.show()
  _update_left_counter(self._selection_count)


func fetch():
  if not is_filled():
    return null
  else:
    return self._selected


func is_filled() -> bool:
  return self._selected.size() != 0


func _on_item_selected(state: bool, item: String) -> void:
  if state:
    assert(self._selected.find(item) == -1)
    self._selected.push_back(item)
  else:
    var pos := self._selected.find(item)
    assert(pos != -1)
    self._selected.remove(pos)
  _update_left_counter(self._selection_count - self._selected.size())
  if _are_buttons_locked and self._selection_count != self._selected.size():
    _unlock_buttons()
  elif not _are_buttons_locked and self._selection_count == self._selected.size():
    _lock_buttons()


func _unlock_buttons() -> void:
  _are_buttons_locked = false
  for node in get_node(N_ITEM_CONTAINER).get_children():
    if self._selected.find(node.item) == -1:
      node.disabled = false


func _lock_buttons() -> void:
  _are_buttons_locked = true
  for node in get_node(N_ITEM_CONTAINER).get_children():
    if self._selected.find(node.item) == -1:
      node.disabled = true


func _update_left_counter(n: int) -> void:
  # todo: Could bypass the node search each type by caching the reference
  get_node(N_CHOICES_LEFT_PATH).text = "%s left" % n
