"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
extends FillingScene

# todo: Make editbox unfocus when clicked outside or enter pressed

const N_FILLING_PATH = "Filling"
const N_DESC_PATH = "Desc"

var _desc: String


func init(args: Dictionary) -> FillingScene:
  if "desc" in args:
    self._desc = args["desc"]
  return self


func _ready() -> void:
  if not self._desc.empty():
    var n_desc := get_node(N_DESC_PATH)
    n_desc.text = self._desc
    n_desc.show()


func fetch():
  if not is_filled():
    return null
  else:
    return get_node(N_FILLING_PATH).text


func is_filled() -> bool:
  return not get_node(N_FILLING_PATH).text.empty()
