"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
extends Button

# state: bool
# value: String
signal selected(state, value)


var item: String


func init(desc: String) -> Button:
  # todo: Should be use `desc` and item of item interchangeably?
  self.item = desc
  text = desc
  set("custom_colors/font_color", System.get_next_color())
  return self


func _ready() -> void:
  System.check(connect("toggled", self, "_on_toggled") == OK)


func _on_toggled(state: bool) -> void:
  emit_signal("selected", state, self.item)
