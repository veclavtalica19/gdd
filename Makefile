# todo: Turn on file logging on release

ifeq ($(OS),Windows_NT)
	GODOT_BIN := $(shell cygpath -u "$(GODOT_BIN)")
endif

ifdef RELEASE
	COMMAND := --export
else
	COMMAND := --export-debug
endif

ifdef OPT_SIZE
	MINIFY_CMD := $(shell which minify)
	WASM_OPT_CMD := $(shell which wasm-opt)
	CLOSURE_COMPILER_CMD := $(shell find closure-compiler*.jar)
	CLOSURE_COMPILER_CMD_FIND_STATUS := $(shell (find closure-compiler*.jar > /dev/null; echo 0) || (echo $$?))
endif

.PHONY: all web windows svgs clean-temp clean

all: web windows

web: graphics
	$(GODOT_BIN) $(COMMAND) "HTML5" ./.build/index.html --no-window
ifeq ($(CLOSURE_COMPILER_CMD_FIND_STATUS),0)
	java -jar $(CLOSURE_COMPILER_CMD) --js ./.build/index.js --compilation_level=ADVANCED --jscomp_off=* --process_common_js_modules --js_output_file ./.build/temp
	cp -f ./.build/temp ./.build/index.js
endif
ifneq ($(MINIFY_CMD),)
	$(MINIFY_CMD) ./.build/index.html > ./.build/temp
	cp -f ./.build/temp ./.build/index.html
	$(MINIFY_CMD) ./.build/index.js > ./.build/temp
	cp -f ./.build/temp ./.build/index.js
	$(MINIFY_CMD) ./.build/index.audio.worklet.js > ./.build/temp
	cp -f ./.build/temp ./.build/index.audio.worklet.js
endif
ifneq ($(WASM_OPT_CMD),)
	$(WASM_OPT_CMD) -Oz ./.build/index.wasm -o ./.build/temp
	cp -f ./.build/temp ./.build/index.wasm
endif
	$(MAKE) clean-temp

windows: svgs
	$(GODOT_BIN) $(COMMAND) "Windows Desktop" ./.build/gdd.exe --no-window

svgs:
	$(MAKE) -C ./resources/art

clean-temp:
	$(RM) -fr ./.build/temp

clean:
	$(MAKE) -C ./resources/art clean
	$(RM) -fr ./.build
