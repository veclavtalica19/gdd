"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
extends Resource
class_name ChoiceContext

enum {
  INVALID,
  DOC_LIST, # List of distinct doc options
  DOC_FILLING, # Filling of context fields of some particular doc option
}

var type := INVALID
var context


func init_doc_list(docs: Array) -> Resource:
  self.type = DOC_LIST
  self.context = docs
  return self


func init_doc_filling(doc: DocOption) -> Resource:
  self.type = DOC_FILLING
  self.context = doc
  return self
