"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
extends Node

# todo: Rename this. Possibly to simply GDD? As in scope that shared functionalities are stored
# todo: User configurable zoom

const DESKTOP_DEFAULT_RESOLUTION = Vector2(1024, 600)
const MOBILE_DEFAULT_RESOLUTION = Vector2(360, 640)
const MOBILE_DEFAULT_SCALE = 1.5

var epsilon: float
var gameplay_random := RandomNumberGenerator.new()
var visual_random := RandomNumberGenerator.new()

var _colors := ProjectSettings.get_setting("global/color_array") as PoolColorArray
var _cur_color := 0


func _enter_tree() -> void:
  check(get_tree().connect("files_dropped", self, "_on_files_dropped") == OK)
  _init_resolution()


func _on_files_dropped(files: PoolStringArray, _monitor: int) -> void:
  print(files)


func _init() -> void:
  # randomize()
  gameplay_random.randomize() # todo: Should be set by game state
  OS.low_processor_usage_mode = true
  epsilon = find_epsilon()


func _init_resolution() -> void:
  # todo: Cache window size changes from runtime and apply them on startup
  # todo: Adapt to device display at hand, not assume as much
  var resolution: Vector2 = DESKTOP_DEFAULT_RESOLUTION
  var scale: float = 1.0

  match OS.get_name():
    "Android", "iOS":
      resolution = MOBILE_DEFAULT_RESOLUTION
    "HTML5":
      var is_mobile := false
      if OS.has_feature('JavaScript'):
        is_mobile = JavaScript.eval("/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)", true) as bool
      else:
        # todo: Fallback on where javascript_eval is stripped off
        pass
      if is_mobile:
        resolution = MOBILE_DEFAULT_RESOLUTION
        scale = 2.0

  OS.set_window_size(resolution)
  get_tree().set_screen_stretch(
    int(ProjectSettings.get_setting("display/window/stretch/mode")),
    int(ProjectSettings.get_setting("display/window/stretch/aspect")),
    resolution,
    scale)


func get_next_color() -> Color:
  # todo: Make it static and operational over Array<Color>
  if self._cur_color >= self._colors.size() - 1:
    self._cur_color = -1
  self._cur_color += 1
  return _colors[_cur_color]


static func rand_seq(rg: RandomNumberGenerator, arg: Array, count: int = -1) -> Array:
  # Takes array and returns N element from it in random
  # If len is not specified then resulting array will be the same size as input, but in random order
  assert(count >= -1 and count <= arg.size())
  if count == -1:
    count = arg.size()
  var result := Array()
  var gray_values := Array() # Refers to indexes that are already in use
  while count != 0:
    var dice := rg.randi() % arg.size()
    if gray_values.find(dice) == -1:
      result.push_back(arg[dice])
      gray_values.push_back(dice)
      count -= 1
  return result


static func find_epsilon() -> float:
  var eps := 1.0
  var eps_last: float
  while 1.0 + eps != 1.0:
    eps_last = eps
    eps /= 2.0
  return eps_last


func do_assert(status: bool, msg: String = "") -> void:
  if not status:
    if msg.empty():
      push_error("{source}:{function}:{line}".format(get_stack()[1]))
    else:
      push_error("{source}:{function}:{line} ".format(get_stack()[1]) + msg)
    # todo: Apparently it crashes on web build?
    #       We could change the scene to crash report instead of exiting
    get_tree().quit(1)


static func check(status: bool, msg: String = "") -> void:
  assert(status, msg)
