"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
extends Resource
class_name DocOption

# todo: Repeating strings shouldn't be individually stored, creating a lot of copies, we possibly need to have string pool of tags
#       Storing by integers also gives lookup and comparison advantage
#       Could be implemented by having singleton of DocOptionTagPool that holds the int to string dictionary
#       And having getters/setters here work on that, instead of direct access

# Holds info string about where the option was defined
var source: String

var label: String
var desc: String
var tags: PoolStringArray

# Nullable Dictionary
var context = null
