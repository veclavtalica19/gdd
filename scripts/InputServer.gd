"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
# Centralized way to set callbacks for TouchInputManager
# It's especially handy in case of gui element consuming the inputs when it's not desirable

# todo: We need way to somehow make events consumable, for example, when SplitContainer is active,
#       then no events should be propagated elsewhere as long as dragging isn't finished 
#       Same with screen_drag subscribers, while dragging is active - all events should go exclusively to active object

extends Node

signal screen_drag


func _input(event: InputEvent) -> void:
  if event is InputEventScreenDrag:
    emit_signal("screen_drag", event)


static func is_within(node: Control, pos: Vector2) -> bool:
  # Handy for checking whether particular positional input is within a node rect
  return Rect2(node.rect_global_position, node.rect_size).has_point(pos)
