"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
# Warn! For energy saving reasons it turns processing on and off on demand, which means defined overload in children might be impacted
# todo: We probably could check whether children class implements _process? And only set it if it doesn't

extends Container
class_name Picker

# picked(arg: Variant)
signal picked

export(NodePath) var progress_hint
onready var n_ProgressHint := get_node_or_null(progress_hint)

# Propagated in picked signal
var _picked_arg = null

const SELECT_PROGRESS_LERP := 0.98
const SELECT_DEPROGRESS_LERP := 0.99
const SELECT_THRESHOLD := 0.95

var _selection_progress: float = 0.0
var _is_being_selected: bool = false


func _ready() -> void:
  System.check(connect("gui_input", self, "_on_input") == OK)


func _process(delta: float) -> void:
  if self._is_being_selected:
    self._selection_progress = lerp(self._selection_progress, 1.0, 1.0 - pow(1.0 - SELECT_PROGRESS_LERP, delta))
  else:
    self._selection_progress = lerp(self._selection_progress, 0.0, 1.0 - pow(1.0 - SELECT_DEPROGRESS_LERP, delta))
  if self._selection_progress >= SELECT_THRESHOLD:
    self._selection_progress = 0.0
    emit_signal("picked", self._picked_arg)
    set_process(false)
  elif self._selection_progress <= 0.01:
    self._selection_progress = 0.0
    set_process(false)
  if n_ProgressHint != null:
    # todo: Would be better to have progression implementation in hint object itself
    self.n_ProgressHint.color.a = self._selection_progress
    self.n_ProgressHint.scale = Vector2(rect_size.x * self._selection_progress, rect_size.y)


func _on_input(event: InputEvent) -> void:
  if event is InputEventMouseButton and event.button_index == BUTTON_LEFT or event is InputEventScreenTouch:
    if self._is_being_selected:
      if not event.pressed:
        self._is_being_selected = false
    elif event.pressed:
      self._is_being_selected = true
      set_process(true)
