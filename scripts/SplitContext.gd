"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
extends Node
class_name SplitContext

enum Orientation { HORIZONTAL, VERTICAL }

export(Orientation) var orientation := VERTICAL setget set_orientation

onready var _property_backup := _fetch_properties()

var _vertical_offset: int = 0
var _horizontal_offset: int = 132


func _ready() -> void:
  var n_Split := $Split as SplitContainer
  System.check(n_Split.connect("dragged", self, "_on_dragged") == OK)


func _fetch_properties() -> Dictionary:
  # Used for fetching particular starting properties that are needed to be preserved on node swapping
  var n_Split := $Split as SplitContainer
  var result := Dictionary()
  for prop in ["size_flags_horizontal", "size_flags_vertical", "rect_position", "rect_size", "anchor_left", "anchor_right", "anchor_top", "anchor_bottom", "margin_left", "margin_right", "margin_top", "margin_bottom"]:
    result[prop] = n_Split.get(prop)
  return result


func set_orientation(orient: int) -> void:
  if orientation == orient:
    return
  orientation = orient    
  var replacement := (VSplitContainer if orient == VERTICAL else HSplitContainer).new()
  for prop in self._property_backup:
    replacement.set(prop, self._property_backup[prop])
  var n_Split := $Split as SplitContainer
  var children := n_Split.get_children()
  for child in children:
    n_Split.remove_child(child)
    replacement.add_child(child)
  n_Split.free()    
  replacement.set_name("Split")
  replacement.split_offset = self._vertical_offset if orient == VERTICAL else self._horizontal_offset
  add_child(replacement)
  System.check(replacement.connect("dragged", self, "_on_dragged") == OK)


func _on_dragged(offset: int) -> void:
  if orientation == VERTICAL:
    self._vertical_offset = offset
  else:
    self._horizontal_offset = offset
