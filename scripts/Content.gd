"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
extends Node

# todo: Caching via serialization so that reevaluation isn't needed each startup
#       We would need to implement file lock tho, based on how recently files that are in use were changed

const PATH_ROOT_PATH := "res://resources/packs"
const DEFAULT_PACK_NAME := "gdd-base"
const MANIFEST_FILE := "Manifest.json"

# Dictionary<DocOptions>
var _ideas: Dictionary
# Dictionary<Array<String>>
var _tag_lists: Dictionary
# Dictionary<Dictionary<Variant>>
var _tag_properties: Dictionary


func _init() -> void:
  load_pack(DEFAULT_PACK_NAME)


func get_ideas() -> Array:
  return self._ideas.keys()


func get_idea(key: String) -> DocOption:
  return self._ideas[key]


func load_pack(name: String) -> void:
  var pack_path := [PATH_ROOT_PATH, name]
  var manifest := load_json_file(make_path(pack_path + [MANIFEST_FILE])) as Dictionary
  if "tag-files" in manifest:
    for filename in qualify_paths(manifest["tag-files"] as Array, pack_path):
      print_debug("Loading tag properties file %s..." % filename)
      _load_tag_info(filename)
  if "option-files" in manifest:
    for filename in qualify_paths(manifest["option-files"] as Array, pack_path):
      print_debug("Loading option file %s..." % filename)
      _load_doc_piece(filename)


static func qualify_paths(args: Array, path: Array) -> Array:
  var result := Array()
  for arg in args:
    result.push_back(make_path(path + [arg]))
  return result


static func make_path(args: PoolStringArray) -> String:
  return args.join('/')


static func files_in_dir(path: String) -> PoolStringArray:
  var result := PoolStringArray()
  var dir := Directory.new()
  if dir.open(path) != OK:
    push_error("Cannot open " + path)
    return result
  if dir.list_dir_begin(true) != OK:
    push_error("Error calling list_dir_begin()")
    return result
  var file = dir.get_next()
  while not file.empty():
    result.push_back(make_path([path, file]))
    file = dir.get_next()
  dir.list_dir_end()
  return result


static func filter_by_extension(files: PoolStringArray, ext: String) -> PoolStringArray:
  var result := PoolStringArray()
  for file in files:
    if file.ends_with(ext):
      result.push_back(file)
  return result


static func concat_arrays_uniques(target: Array, param: Array) -> void:
  for arg in param:
    if target.find(arg) == -1:
      target.push_back(arg)


func expand_tags(tags: Array) -> Array:
  var result := Array()
  for tag in tags:
    match tag[0]:
      "#":
        # Expands to tag sequence that is defined in Tags.json
        assert(tag in self._tag_lists)
        concat_arrays_uniques(result, self._tag_lists[tag])
      _:
        result.push_back(tag)
  return result


static func load_json_file(filename: String):
  var file := File.new()
  if file.open(filename, File.READ) != OK:
    push_error("Cannot open file %s" % filename)
    return null
  var json_result := JSON.parse(file.get_as_text())
  file.close()
  if json_result.error != OK:
    push_error(json_result.error_string)
    return null
  return json_result.result


func _load_doc_piece(filename: String) -> void:
  var content := load_json_file(filename) as Dictionary
  for idea in content:
    var option := DocOption.new()
    option.source = "%s:%s" % [filename, idea] # todo: Only assign on Debug? Tho reflective capabilities might be needed for packs later
    for field in ["label", "desc"]:
      if field in content[idea]:
        assert(
          content[idea][field] is String,
          "%s:%s field must be of type String" % [option.source, field])
        option.set(field, content[idea][field])
    for field in ["tags"]:
      if field in content[idea]:
        assert(
          content[idea][field] is Array,
          "%s:%s field must be of type Array" % [option.source, field])
        # todo: Investigate whether assigning array to pool*array will make new alloc and copy everything
        option.set(field, expand_tags(content[idea][field]))
    if "context" in content[idea]:
      assert(
        content[idea]["context"] is Array,
        "%s:context field must be of type Array" % option.source)
      option.context = content[idea]["context"]
    self._ideas[idea] = option


func _load_tag_info(filename: String) -> void:
  # todo: Check whether Tags.json exist, inability to open existing file should be different from blind attempt to open nothing
  var file := File.new()
  if file.open(filename, File.READ) != OK:
    push_warning("Couldn't read Tags.json")
    return
  var json_result := JSON.parse(file.get_as_text())
  file.close()
  if json_result.error != OK:
    push_error(json_result.error_string)
    return
  var tag_info := json_result.result as Dictionary
  for tag in tag_info:
    match tag[0]:
      "#":
        # Tag list, used for grouping of related tags, especially useful in selections
        assert(not tag in self._tag_lists, "Redefinition of %s tag group" % tag)
        self._tag_lists[tag] = tag_info[tag]
      _:
        # Everything else is generic tag where each key pair is property describing some relation
        # todo: Allow redefinition by merging properties?
        assert(not tag in self._tag_properties, "Redefinition of %s tag properties" % tag)
        self._tag_properties[tag] = tag_info[tag]
