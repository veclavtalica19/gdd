"""
gdd
Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
extends Resource
class_name Project

# Possible types: flavor | option
# Flavor is in-development commentary from your team
# Option is something that was picked to be shaping of the game
var _history: Array


func add_flavor(text: String) -> void:
  self._history.push_back({"type": "flavor", "content": text})


func add_option(opt: DocOption, context = null) -> void:
  self._history.push_back({"type": "option", "content": opt, "context": context})
