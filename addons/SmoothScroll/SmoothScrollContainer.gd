## Smooth scroll functionality for ScrollContainer
##
## Applies velocity based momentum and "overdrag"
## functionality to a ScrollContainer
extends ScrollContainer
class_name SmoothScrollContainer

# todo: Fix noticeable jump when scrolling from overdragging
# todo: Fix sling effect on overdragging, that results in overshooting
# todo: Test horizontal scrolling and apply changes to accommodate it if needed
# todo: Option to snap to items of child containter, so that top-most element should be fully visible if it can be
# todo: Support for other devices, currently it's relying on mouse wheel scroll
# todo: Support for multiple control children
# todo: Inversed lerp: there should be immediate jump on receiving scrol input, but only the last part of motion should be smoothed, it will give it a lot snappier feel compared to current method
# todo: Option for scrolling by pressing and pointer device movement
# todo: Make scroll_to_bottom() smooth, as any other function. Target based scrolling will make it easier
# todo: Call set_process() to reflect whether _process() should be repeatedly called based on whether processing is needed
# todo: Optionally turn off overdrag

enum { NONE, DOWN, UP }

# Movement too small to register as such
const GESTURE_SNAP_UNDER = 2.0

# Drag impact in pixels per second
export(float) var speed = 100.0
# Gesture travel to velocity
export(float) var gesture_speed = 0.8
# Softness of damping when "overdragging", in percent
export(float, 0.01, 1.0) var damping = 0.95
# Stops movement when velocity magnitude is lower than or equal to
export(float, 0.01, 1.0) var snap_under = 0.2
# Friction value
export(float) var friction = 1.0

# Current velocity of the `content_node`
var _velocity := Vector2.ZERO
# Control node to move when scrolling
var _content_node: Control = null
# Current position of `content_node`
var _pos := Vector2.ZERO
# When true, `content_node`'s position is set by dragging the scroll bar
var _is_bar_scrolling := false
# Direction of vertical scroll
var _vertical_scroll_dir := NONE


func _ready() -> void:
  System.check(InputServer.connect("screen_drag", self, "_on_screen_drag") == OK)
  System.check(get_v_scrollbar().connect("scrolling", self, "_on_VScrollBar_scrolling") == OK)
  for c in get_children():
    self._content_node = c


func _process(delta: float) -> void:  
  # If no scroll needed, don't apply forces
  #  if self._content_node.rect_size.y - self.rect_size.y < 1:
  #    set_process(false)
  #    return

  match self._vertical_scroll_dir:
    DOWN:
      self._velocity.y -= self.speed * delta
    UP:
      self._velocity.y += self.speed * delta

  # Distance between content_node's bottom and bottom of the scroll box 
  var bottom_distance:= self._content_node.rect_position.y + self._content_node.rect_size.y - self.rect_size.y
  # Distance between content_node and top of the scroll box
  var top_distance:= self._content_node.rect_position.y
  
  # Simulate friction
  self._velocity /= 1.0 + self.friction * delta
  
  # Applies counterforces when overdragging
  if bottom_distance < 0:
    self._velocity.y = lerp(self._velocity.y, -bottom_distance/8, 1.0 - pow((1.0 - self.damping), delta))
  if top_distance > 0:
    self._velocity.y = lerp(self._velocity.y, -top_distance/8, 1.0 - pow((1.0 - self.damping), delta))

  # If velocity is too low, don't affect the position by it
  if self._velocity.length() > self.snap_under:
    self._pos += self._velocity
#  else:
#    set_process(false)
  
  self._content_node.rect_position = self._pos
  self._vertical_scroll_dir = NONE
  set_v_scroll(-self._pos.y)


func _gui_input(event: InputEvent) -> void:
  if not event is InputEventMouseButton:
    return

  if not event.is_pressed():
    self._is_bar_scrolling = false

  match event.button_index:
    BUTTON_WHEEL_DOWN:
      if self._velocity.y > 0.0:
        self._velocity.y = 0.0
      self._vertical_scroll_dir = DOWN
      set_process(true)
    BUTTON_WHEEL_UP:
      if self._velocity.y < 0.0:
        self._velocity.y = 0.0
      self._vertical_scroll_dir = UP
      set_process(true)


func _on_screen_drag(event: InputEventScreenDrag) -> void:
  if abs(event.relative.y) <= GESTURE_SNAP_UNDER:
    return

  if not InputServer.is_within(self, event.position):
    return

  if event.relative.y > 0.0:
    if self._velocity.y > 0.0:
      self._velocity.y = 0.0
  else:
    if self._velocity.y < 0.0:
      self._velocity.y = 0.0

  self._velocity.y += event.relative.y * gesture_speed
  set_process(true)


func _on_VScrollBar_scrolling() -> void:
  self._is_bar_scrolling = true
  self._pos = self._content_node.rect_position
  self._velocity = Vector2.ZERO
  set_process(false)


# Adds velocity to the vertical scroll
func scroll_vertical(amount: float) -> void:
  self._velocity.y -= amount
  set_process(true)


# Scrolls to top
func scroll_to_top() -> void:
  # Reset velocity
  self._velocity.y = 0
  # Move content node to top
  self._pos.y = 0
  self._content_node.rect_position = self._pos
  # Update vertical scroll bar
  set_v_scroll(-self._pos.y)


# Scrolls to bottom
func scroll_to_bottom() -> void:
  # todo: Currently doesn't scroll to the very end
  # todo: Force the resizing of itself/content_node? As it's possible that rect changes are deferred
  # Reset velocity
  self._velocity.y = 0
  self._vertical_scroll_dir = NONE
  # Move content node to bottom
  self._pos.y = -self._content_node.rect_size.y + self.rect_size.y
  self._content_node.rect_position = self._pos
  # Update vertical scroll bar
  set_v_scroll(-self._pos.y)
