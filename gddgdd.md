---
subtitle: Game development game inspiring imagination.
title: GDD
---

Main mechanic is the way projects are defined. Instead of most other
tycoons there's no complex multi-stage interfaces with predetermined
options, but instead narrative-like flow of writing of imaginary game
design document. This document hints to aspects of development that are
considered important while allowing colorful description. At the same
time game should provoke the imagination, but not dictate what exactly
to imagine. Being vague is good for implementation of things too.

All ideas described should come from virtual people that are creating
have their personalities and own small stories. It would be most
beneficial for engagement to have stories with actors, - heroes and
villains, as it's natural for human to get attached to them.

Actual gameplay comes in multiple forms, - managing and realizing the
laid-out docs and managing people's ambitions. Sometimes picking what
feels best might touch feelings and pride of people and player might be
interested in keeping some people around, which could create dilemmas.

Process of creating the doc:

-   Player is presented with finite amount of options for game to be
    based on. Top-most statement is the most meaningful and will impact
    which options will be presented later, having some sense of
    coherency.

-   Each option has authorship and opinions of other people on it.

-   Order of topics might vary. For example, sometimes story decisions
    might predate the gameplay ones or reverse of that.

-   Process is dream-y and ambitious, with goals that are desirable and
    creative.

-   In essence each option is shaping out the ambiguity. If something
    isn't stated, or isn't inferred by the player -- they have the
    authority on picturing it in whatever way they see fit. Game should
    try to suggest player to fill the gaps; to make engaged in creation.

-   For liberty of expression options shouldn't generally dictate the
    complexity of mechanical complexity of realization stage, and when
    they do it should be obvious.

-   Only at the very end player is asked to give name to the project,
    finalizing everything by it.

-   Some ideas are asking for player to express something by themselves,
    for example by giving names, composing tags, icons (akin to book
    writing in Sims 2) or doodles. Doodle drawing is really limited,
    just one color, button to erase everything and retry and that's it.

Process of realizing the doc:

-   Deliberate contrast with imagination of doc creation. Reality is
    harsh and unforgiving and player team will inevitably be challenged.
    It's mechanical, but also a gamble, which is showed in card-inspired
    gameplay.

-   Realization of project is done by dealing with incoming problems.
    What's great about this approach is that many problems are universal
    in game development, and thus you don't need to create a lot of
    content for each idea, that's very relevant in mechanics driven
    stage. Of course, there should be things that are brought to player
    via his design choices though.

-   Options are treated as cards that are played in turns, each turn
    representing timeline.

-   ~~Different ones might have different effect on development. For
    example, 3D graphics means that content options might require 3D
    artists and some engine related work to accommodate that. How
    character visuals are done might depend on narrative options, which
    will require additional work if dependency is changed along the
    development.~~

-   Each option card has people designations. Opinions of people working
    with each other might affect the result. What should be important is
    that for player it shouldn't be required to follow interfaces for
    keeping arbitrary numbers of "compatibility" in check, rather
    stories that are happening here and there should suggest what needs
    to be done. Player should know their workers first, emotionally. For
    that there should be thematical reasonable limit on how many people
    you might need to manage. High counts could just kill any hope of
    keeping up with character stories and personalities.

Doc miscellaneous details:

-   Each doc page should have visual studio ownership notice as small
    hint of player expression.

-   There might be rare jokes and expressive remarks left on pages from
    individual, we should try keeping it alive and breathing.

-   Descriptions of options are always coming from somebody, so they're
    in first person speech.

-   Give player the ability to choose font of gdd page sometimes.

Thematic overview:

~~Game takes place in alternative universe which technological and
society state is similar to 80s-90s. Especially when it comes to how
game businesses were shaped, aka, with great impact of individuals and
small amounts of people on each individual project. It's important for a
game feel which is deliberately anti-corporate, or rather anti profit
maximization.~~

~~Culturally and visually it should preferably not be cliché North
American gamedev/gaming culture of the era, as it's hugely associated
with gaming history already. It might be with hints of Asian of Eastern
Block flavor.~~

Adapting to realities of player seems to be more elegant. We shouldn't
be too specific, but describe things in ways that are as much universal
as possible.

Actors:

Each person in a team has certain ambitions. They manifest in some idea
about games in which they strongly believe. Most of them are based on
genre archetypes, for example, one might value player expression,
believing that games should be based around sandboxes which are built
for experimentation. Other might view games as ultimate medium for
narrative, cinematic experiences.

At the same time other people could have visions based on settings and
visuals. (Thought it might be problematic to express for the player as
we can't really have anything visual to showcase it and text
descriptions might be too vague. On the other hand, vagueness might be
our friend)

Funny stuff:

-   Actor that continuously brings the same idea over and over again.

Artefacts:

Things on which player spent their time and imagination should try to
be recurrent. Hero of one game might be an easter egg in other game,
great universe or storyline might be continued and expanded in new
context and etc.
